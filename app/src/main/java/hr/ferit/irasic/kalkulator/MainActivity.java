package hr.ferit.irasic.kalkulator;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity  {

    EditText editText;
    EditText editText2;
    TextView textView;
    Button button_plus;
    Button button_minus;
    Button button_puta;
    Button button_dijeli;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.initializeUI();
    }

    private void initializeUI()
    {
        this.editText = (EditText) findViewById(R.id.editText);
        this.editText2 = (EditText) findViewById(R.id.editText2);
        this.textView = (TextView) findViewById(R.id.textView);
        this.button_plus = (Button) findViewById(R.id.buttonadd);
        this.button_minus = (Button) findViewById(R.id.buttonsub);
        this.button_puta = (Button) findViewById(R.id.buttonputa);
        this.button_dijeli = (Button) findViewById(R.id.buttondijeli);

        this.button_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fnumber1 = editText.getText().toString();
                double snumber1 = Double.parseDouble(fnumber1);
                String fnumber2 = editText2.getText().toString();
                double snumber2 = Double.parseDouble(fnumber2);
                double add = snumber1+snumber2;
                textView.setText(String.valueOf(add));
            }
        });
        this.button_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fnumber1 = editText.getText().toString();
                double snumber1 = Double.parseDouble(fnumber1);
                String fnumber2 = editText2.getText().toString();
                double snumber2 = Double.parseDouble(fnumber2);
                double sub = snumber1-snumber2;
                textView.setText(String.valueOf(sub));
            }
        });
        this.button_puta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fnumber1 = editText.getText().toString();
                double snumber1 = Double.parseDouble(fnumber1);
                String fnumber2 = editText2.getText().toString();
                double snumber2 = Double.parseDouble(fnumber2);
                double multi = snumber1*snumber2;
                textView.setText(String.valueOf(multi));

            }
        });
        this.button_dijeli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fnumber1 = editText.getText().toString();
                double snumber1 = Double.parseDouble(fnumber1);
                String fnumber2 = editText2.getText().toString();
                double snumber2 = Double.parseDouble(fnumber2);
                double dijeli = snumber1/snumber2;
                textView.setText(String.valueOf(dijeli));

            }
        });

    }

}